import AdminLayout from "../Layouts/AdminLayout";
import Layout from "../Layouts/Layout";
import AdminJobsPage from "../Pages/AdminJobsPage/AdminJobsPage";
import AdminServicesPage from "../Pages/AdminServicesPage/AdminServicesPage";
import AdminTypesOfJobPage from "../Pages/AdminTypesOfJobPage/AdminTypesOfJobPage";
import AdminUserPage from "../Pages/AdminUserPage/AdminUserPage";
import CategoriesPage from "../Pages/CategoriesPage/CategoriesPage";
import HomePage from "../Pages/HomePage/HomePage";
import JobDetailPage from "../Pages/JobDetailPage/JobDetailPage";
import LoginPage from "../Pages/LoginPage/LoginPage";
import ProfilePage from "../Pages/ProfilePage/ProfilePage";
import RegisterPage from "../Pages/RegisterPage/RegisterPage";
import ResultPage from "../Pages/ResultPage/ResultPage";
import TitlePage from "../Pages/TitlePage/TitlePage";

export const userRoute = [
    // * Đây là nơi chứa các thông tin của các route liên quan tới các trang page dành cho user 
    {
        path: '/',
        component: <Layout Component={HomePage} />
    },
    {
        path: '/login',
        component: <Layout Component={LoginPage} />
    },
    {
        path: '/register',
        component: <Layout Component={RegisterPage} />
    },
    {
        path: '/profile',
        component: <Layout Component={ProfilePage} />
    },
    {
        path: '/title/:id',
        component: <TitlePage />
    },
    {
        path: '/jobDetail/:id',
        component: <JobDetailPage/>
    },
    {
        path: '/categories/:id',
        component: <CategoriesPage />
    },
    {
        path: '/result/:id',
        component: <Layout Component={ResultPage} />
    },
    {
        path: '/admin-users',
        component: <AdminLayout Component={AdminUserPage} />
    },
    {
        path: '/admin-jobs',
        component: <AdminLayout Component={AdminJobsPage} />
    },
    {
        path: '/admin-types-of-job',
        component: <AdminLayout Component={AdminTypesOfJobPage} />
    },
    {
        path: '/admin-services',
        component: <AdminLayout Component={AdminServicesPage} />
    },
]