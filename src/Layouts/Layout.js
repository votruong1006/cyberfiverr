import React from 'react'
import Header from '../Components/Header/Header'
import Footer from '../Components/Footer/Footer'
import CategoriesMenu from '../Components/CategoriesMenu/CategoriesMenu'

export default function Layout({ Component }) {
    return (
        <div className='h-full min-h-screen flex flex-col'>
            <Header/>
            <CategoriesMenu/>
            <div className='flex-grow mt-0'>
                <Component />
            </div>
            <Footer />
        </div>
    )
}
