import React from 'react'
import { Dropdown , Space } from 'antd'
import { NavLink } from 'react-router-dom'
import { useDispatch } from 'react-redux'

function ItemMenuDropdown({work}) {
    let dispatch = useDispatch() ; 
    let renderTypeWorkDetail = () => {
        return work.dsNhomChiTietLoai.map((workDetail) => {
            return {
                key : workDetail.id , 
                label : (
                    <div>
                        <p className='font-bold mb-3'>{workDetail.tenNhom}</p>
                        {workDetail.dsChiTietLoai.map((typeWork , index)=> {
                            return <NavLink to={`/categories/${typeWork.id}`} className='block mb-2 menu-item__dropdown' key={index}>{typeWork.tenChiTiet}</NavLink>
                        })}
                    </div>
                )
            }
        })
    }
    return (
        <div className='text-center categoriesItem'>
            <Dropdown
            menu={{
                items : renderTypeWorkDetail() 
            }}
            >
                <NavLink className='menu__dropdown text-base' to={`/title/${work.id}`}>{work.tenLoaiCongViec}</NavLink>
            </Dropdown>
        </div>
    )
}

export default ItemMenuDropdown