import React from 'react'
import Modal from 'react-modal';
import { useDispatch, useSelector } from 'react-redux';
import { handleCloseModal } from '../../Toolkits/modalSlice';
import '../../css/modal.css'

Modal.setAppElement('#root');
function ModalPopUp() {
    let dispatch = useDispatch();
    let handleClose = () => {
        dispatch(handleCloseModal());
    }

    let { url, isOpen } = useSelector(state => state.modalSlice);
    return (
        <Modal style={{ overlay: { zIndex: 1000 }, content: { background: 'rgb(0, 0, 0 , .75)' } }} className='h-screen w-screen' isOpen={isOpen} onRequestClose={handleClose} >
            <div className='p-14'>
                <div className='modal__container'>
                    <video autoPlay controls src={url}></video>
                    <button className='modal-close' onClick={handleClose}>X</button>
                </div>
            </div>
        </Modal>
    )
}

export default ModalPopUp;
