import { CaretDownOutlined, EditFilled } from "@ant-design/icons";
import { Button, Dropdown, Modal, Space, message } from "antd";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import ModalProfile from "../../Components/ModalProfile/ModalProfile";
import { localUserServ } from "../../Services/localService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from 'react-redux'
import { setHomePage } from '../../Toolkits/HomeSlice';
export default function ProfilePage() {
    const navigate = useNavigate()
    let dispatch = useDispatch();
    dispatch(setHomePage());
    const handleLogout = () => {
        localUserServ.remove()
        navigate("/")
        message.success("Đăng xuất thành công !")
    }
    const { user } = useSelector((state) => state.userSlice.userInfo);
    const items = [
        {
            label: <button onClick={handleLogout}>Đăng xuất</button>,
            key: "0",
        },
    ];

    return (
        <div style={{ marginTop: '150px', marginBottom: '48px' }}>
            <div className="flex w-full max-w-screen-xl mx-auto container">
                <div style={{}} className="profile__left">
                    <div className="mb-7">
                        <div
                            style={{ border: "1px solid #dadbdd" }}
                            className="p-8 text-sm  border-solid"
                        >
                            <div className="flex justify-between">
                                <Dropdown
                                    menu={{
                                        items,
                                    }}
                                    trigger={["click"]}
                                >
                                    <a onClick={(e) => e.preventDefault()}>
                                        <Space>
                                            <CaretDownOutlined />
                                        </Space>
                                    </a>
                                </Dropdown>
                                <div
                                    style={{ border: "1px solid #1dbf73" }}
                                    className="rounded-xl border-solid border-green-500 px-2 text-green-500"
                                >
                                    . Online
                                </div>
                            </div>
                            <div className="">
                                <div className="flex flex-col items-center">
                                    <p className="font-bold block text-xl">{user ? user.email : null}</p>
                                    <div className="">
                                        <ModalProfile />
                                    </div>
                                </div>
                            </div>
                            <div
                                style={{ borderTop: "1px solid #dadbdd" }}
                                className="mt-6 pt-5 "
                            >
                                <div className="flex justify-between mb-4">
                                    <div className="text-sm text-gray-500">
                                        <i class="fa fa-map-marker-alt"></i>
                                        <span className="ml-3">From</span>
                                    </div>
                                    <span className="text-sm font-bold text-gray-600">
                                        Vietnam
                                    </span>
                                </div>
                                <div className="flex justify-between mb-4">
                                    <div className="text-sm text-gray-500">
                                        <i class="fa fa-user"></i>
                                        <span className="ml-3">Member since</span>
                                    </div>
                                    <span className="text-sm font-bold text-gray-600">
                                        Oct2022
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <div
                            style={{ border: "1px solid #dadbdd" }}
                            className="p-8 text-sm  border-solid"
                        >
                            <div
                                style={{ borderBottom: "1px solid #ddd" }}
                                className="mb-8 pb-2"
                            >
                                <div className="flex items-center justify-between">
                                    <h3 className="font-bold text-base">Decription</h3>
                                    <ModalProfile />
                                </div>
                                <div className="flex items-center gap-12 text-gray-500">
                                    <h6 className="w-2/5 text-base font-light mb-2">Name:</h6>
                                    <p className="text-sm pt-2 mb-3">{user ? user.name : null}</p>
                                </div>
                                <div className="flex items-center gap-12 text-gray-500">
                                    <h6 className="w-2/5 text-base font-light mb-2">Phone:</h6>
                                    <p className="text-sm pt-2 mb-3">{user ? user.phone : null}</p>
                                </div>
                                <div className="flex items-center gap-12 text-gray-500">
                                    <h6 className="w-2/5 text-base font-light mb-2">Birthday:</h6>
                                    <p className="text-sm pt-2 mb-3">{user ? user.birthday : null}</p>
                                </div>
                            </div>
                            <div
                                style={{ borderBottom: "1px solid #ddd" }}
                                className="mb-8 pb-2"
                            >
                                <div className="font-bold text-base">
                                    <h3>Languages</h3>
                                </div>
                                <p className="text-sm pt-2 mb-3 text-gray-500">
                                    English - <span className="text-gray-300">Basic</span>
                                </p>
                                <p className="text-sm pt-2 mb-3 text-gray-500">
                                    Vietnamese (Tiếng Việt) -{" "}
                                    <span className="text-gray-300"> Native/Bilingual</span>
                                </p>
                            </div>
                            <div
                                style={{ borderBottom: "1px solid #ddd" }}
                                className="mb-8 pb-2"
                            >
                                <div className="flex justify-between items-center">
                                    <h3 className="font-bold text-base">Skill</h3>
                                    <ModalProfile />
                                </div>
                                <div className="">
                                    <p className="text-sm pt-2 mb-3 text-gray-500">
                                        {user ? user.skill : null}
                                    </p>
                                </div>
                            </div>
                            <div
                                style={{ borderBottom: "1px solid #ddd" }}
                                className="mb-8 pb-2"
                            >
                                <div className="flex justify-between items-center">
                                    <h3 className="font-bold text-base">Education</h3>
                                    <ModalProfile />
                                </div>
                                <p className="text-sm pt-2 mb-3 text-gray-500">CYBERSOFT</p>
                            </div>
                            <div
                                style={{ borderBottom: "1px solid #ddd" }}
                                className="mb-8 pb-2"
                            >
                                <div className="flex justify-between items-center">
                                    <h3 className="font-bold text-base">Certification</h3>
                                    <ModalProfile />
                                </div>
                                <p className="text-sm pt-2 mb-3 text-gray-500">
                                    {user ? user.certification : null}
                                </p>
                            </div>
                            <div className="mb-8 pb-2">
                                <div className="flex justify-between items-center">
                                    <h3 className="font-bold text-base">Linked Accounts</h3>
                                </div>
                                <ul className="mt-2">
                                    <li className="mb-1"><i class="fab fa-facebook"></i> <a style={{ textDecoration: "none" }} className="ml-2 text-blue-400 " href="">Facebook</a></li>
                                    <li className="mb-1"><i class="fab fa-google"></i> <a style={{ textDecoration: "none" }} className="ml-2 text-blue-400 " href="">Google</a></li>
                                    <li className="mb-1"><i class="fab fa-github"></i> <a style={{ textDecoration: "none" }} className="ml-2 text-blue-400 " href="">Github</a></li>
                                    <li className="mb-1"><i class="fab fa-twitter"></i> <a style={{ textDecoration: "none" }} className="ml-2 text-blue-400 " href="">Twitter</a></li>
                                    <li className="mb-1"><i class="fa fa-plus hover:text-green-400"></i> <a style={{ textDecoration: "none" }} className="ml-2 text-blue-400 " href="">Dirbble</a></li>
                                    <li className="mb-1"><i class="fa fa-plus hover:text-green-500"></i> <a style={{ textDecoration: "none" }} className="ml-2 text-blue-400 " href="">Stack Overflow</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div style={{}} className="pl-5 profile__right">
                    <div className="mb-7">
                        <div
                            style={{ border: "1px solid #dadbdd" }}
                            className="flex justify-between items-center p-7  border-solid border-gray-950 "
                        >
                            <span style={{ width: "70%" }} className="text-lg font-semibold">
                                It seems that you don't have any active Gigs.
                            </span>
                            <button
                                style={{ width: "30%" }}
                                className="text-lg py-1 rounded bg-green-600 font-bold text-white cursor-pointer"
                            >
                                Create a new Gig
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
