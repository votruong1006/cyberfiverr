import React from 'react'
import JobsTable from './JobsTable/JobsTable'
import AddEditJobsModal from './AddEditJobsModal/AddEditJobsModal'

export default function AdminJobsPage() {
    return (
        <div>
            <AddEditJobsModal />
            <JobsTable />
        </div>
    )
}
