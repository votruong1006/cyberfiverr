import React, { useState } from 'react'
import { Button, Modal, Form, Input, message, Radio } from 'antd';
import '../../../css/addEditModal.css'
import { useDispatch, useSelector } from 'react-redux';
import { handleCloseModal, showAddModal } from '../../../Toolkits/adminSlice';
import { adminServ } from '../../../Services/adminService';

export default function AddEditJobsModal() {
    let dispatch = useDispatch();
    let { isOpenM, jobInfo } = useSelector(state => state.adminSlice)

    const handleAddJob = () => {
        dispatch(showAddModal());
    };
    const handleCancel = () => {
        dispatch(handleCloseModal());
        window.location.reload();
    };

    const onFinish = (values) => {
        if (jobInfo) {
            adminServ.updateJob(values.id, values)
                .then((res) => {
                    message.success('Cập nhật công việc thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                    window.location.reload();
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }
        else {
            adminServ.addJob(values)
                .then((res) => {
                    message.success('Thêm công việc thành công');
                    setTimeout(() => {
                        window.location.reload();
                    }, 1000);
                })
                .catch((err) => {
                    message.error("Đã có lỗi xảy ra");
                });
        }

    };
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div id='addEditJobModal' className='addEditModal'>
            <Button onClick={handleAddJob} className='add'>
                Thêm công việc
            </Button>
            <Modal title={jobInfo ? "Chỉnh sửa thông tin công việc" : "Thêm công việc mới"} open={isOpenM} footer={false} onCancel={handleCancel}>
                <Form
                    name="basic"
                    labelCol={{
                        span: 10,
                    }}
                    wrapperCol={{
                        span: 24,
                    }}
                    style={{
                        maxWidth: 600,
                    }}
                    initialValues={{
                        id: jobInfo ? jobInfo.id : 0,
                        tenCongViec: jobInfo ? jobInfo.tenCongViec : null,
                        maChiTietLoaiCongViec: jobInfo ? jobInfo.maChiTietLoaiCongViec : null,
                        giaTien: jobInfo ? jobInfo.giaTien : null,
                        saoCongViec: jobInfo ? jobInfo.saoCongViec : null,
                        hinhAnh: jobInfo ? jobInfo.hinhAnh : null,
                        moTaNgan: jobInfo ? jobInfo.moTaNgan : null,
                        moTa: jobInfo ? jobInfo.moTa : null,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="off"
                    layout='vertical'
                >
                    <Form.Item
                        label="ID"
                        name="id"
                        rules={[
                            {
                                required: false,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                        className='userID'
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Tên công việc"
                        name="tenCongViec"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Mã chi tiết loại công việc"
                        name="maChiTietLoaiCongViec"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Giá tiền"
                        name="giaTien"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },

                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Sao công việc"
                        name="saoCongViec"
                        rules={[
                            {
                                required: true,
                                message: 'Mục không được để trống!',
                            },

                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Link hình ảnh"
                        name="hinhAnh"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Mô tả ngắn"
                        name="moTaNgan"
                        rules={[
                            {
                                required: true,
                            },
                        ]}
                    >
                        <Input.TextArea style={{ height: '100px' }} />
                    </Form.Item>

                    <Form.Item
                        label="Mô tả chi tiết"
                        name="moTa"
                        rules={[
                            {
                                required: true,
                            },
                        ]}

                    >
                        <Input.TextArea style={{ height: '150px' }} />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button htmlType="submit" className='addEditBtn mr-3'>
                            {jobInfo ? "Cập nhật" : "Thêm"}
                        </Button>
                        <Button onClick={handleCancel}>Hủy</Button>
                    </Form.Item>
                </Form>
            </Modal>
        </div>
    )
}
