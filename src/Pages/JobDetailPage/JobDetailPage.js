import React, { useEffect, useState } from 'react'
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom'
import { setHomePage } from '../../Toolkits/HomeSlice';
import Header from '../../Components/Header/Header';
import CategoriesMenu from '../../Components/CategoriesMenu/CategoriesMenu';
import { workServ } from '../../Services/WorkService';
import { Rate } from 'antd';
import '../../css/titlePage.css';
import Footer from '../../Components/Footer/Footer';
import { Collapse, Progress, Input } from 'antd';
import { localUserServ } from '../../Services/localService';
import Checkout from '../../Components/Checkout/Checkout';
import { configHeader } from '../../Services/config';
const { Panel } = Collapse;
const { Search } = Input;
const { TextArea } = Input;
export default function JobDetailPage() {
    // const onChange = (e) => {
    //     console.log('Change:', e.target.value);
    // };
    const [rating, setRating] = useState(0)
    const [comment, setComment] = useState(" ")
    const [listComment, setListComment] = useState([]);
    const handleRatingChange = (value) => {
        setRating(value)
        console.log(value);
    }
    const handleTextComment = (e) => {
        setComment(e.target.value);
        console.log(e.target.value);
    }
    const [detailedJob, setDetailedJob] = useState({
        congViec: ""
    })
    let dispatch = useDispatch();
    dispatch(setHomePage());
    let params = useParams();
    let id = params.id;
    let fetchDetailedJob = async () => {
        try {
            let res = await workServ.getDetailedJob(id);
            setDetailedJob(res.data.content[0]);
            console.log(res.data.content[0]);
        } catch (error) {
            console.log(error);
        }
    }
    let fetchCommentJob = async () => {
        try {
            let res = await workServ.getCommentJob(id);
            console.log(res.data.content);
            setListComment(res.data.content);
        } catch (error) {
            console.log(error);
        }
    }
    useEffect(() => {
        fetchDetailedJob();
        fetchCommentJob();
        configHeader() ; 
    }, [id])
    let handlePostComent = async () => {
        let userInfo = localUserServ.get();
        console.log(userInfo);
        if (userInfo) {
            try {
                let data = {
                    maCongViec: id,
                    noiDung: comment,
                    saoBinhLuan: rating,
                    maNguoiBinhLuan: userInfo.user.id,
                    ngayBinhLuan: new Date(),
                }
                let res = await workServ.postComment(data)
                console.log(res.data.content);
            } catch (error) {
                console.log(error);
            }
        }
        fetchCommentJob();
    }
    return (
        <>
            <Header />
            <CategoriesMenu />
            <div className="job-detail py-48">
                <div className="container flex flex-wrap justify-between">
                    <div className="title_left">
                        <h1 className='text-3xl font-bold mb-3'>{detailedJob.congViec.tenCongViec}</h1>
                        <div className="seller__overview flex items-center gap-x-3 flex-wrap">
                            <img style={{ borderRadius: '50%' }} className='w-8' src={detailedJob.avatar} alt="img_admin" />
                            <span className='hover:underline cursor-pointer font-bold'>{detailedJob.tenNguoiTao}</span>
                            <span className='text-gray-500'>Level 2 seller</span>
                            <div className='bg-gray-200 h-4 w-0.5'></div>
                            <Rate disabled defaultValue={3} />
                            <span className='text-gray-500'>({detailedJob.congViec.danhGia})</span>
                            <div className='bg-gray-200 h-4 w-0.5'></div>
                            <span className='text-gray-500'>2 Order in Queue</span>
                        </div>
                        <div className='job__image my-3'>
                            <img className='w-full' src={detailedJob.congViec.hinhAnh} alt="" />
                        </div>
                        <div className="job__description">
                            <h2 className='text-xl font-bold text-gray-800 my-3'>About This Git</h2>
                            <p className='text-lg text-gray-600 border-b pb-5' style={{ lineHeight: '27px' }}>{detailedJob.congViec.moTa}</p>
                        </div>
                        <div className="about__seller my-5">
                            <h2 className='text-xl font-bold text-gray-700 mb-5'>About The Seller</h2>
                            <div className="info__seller flex">
                                <div className="image__seller mr-5">
                                    <img style={{ borderRadius: '50%' }} className='w-24' src={detailedJob.avatar} alt="img_seller" />
                                </div>
                                <div className="description__seller space-y-2">
                                    <h3 className='text-gray-600 font-bold'>Admin</h3>
                                    <span className="job__title block">Social Media Advertising</span>
                                    <div>
                                        <Rate disabled defaultValue={3} />
                                        <span className='text-gray-500'>({detailedJob.congViec.danhGia})</span>
                                    </div>
                                    <button className='py-2 px-3 border-2 rounded border-gray-400 text-gray-700 hover:bg-slate-600 hover:text-white hover:border-slate-600'>Contact me</button>
                                </div>
                            </div>
                        </div>
                        <div className="faq__seller my-5">
                            <h3 className='text-gray-600 font-bold mb-4'>FAQ</h3>
                            <div className="fap__content space-y-5">
                                <Collapse>
                                    <Panel header="There are many passages but the majority?" key="1">
                                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Iste porro architecto quod! Alias veritatis reiciendis numquam cupiditate quam soluta delectus porro, perspiciatis libero mollitia. Distinctio aspernatur eaque blanditiis ratione reprehenderit.</p>
                                    </Panel>
                                </Collapse>
                                <Collapse>
                                    <Panel header="There are many passages but the majority?" key="2">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Deserunt iusto libero dolor illo delectus nisi ducimus distinctio eligendi quae ipsa expedita, aliquam sunt rerum tempore autem? Deserunt deleniti non provident.</p>
                                    </Panel>
                                </Collapse>
                                <Collapse>
                                    <Panel header="There are many passages but the majority?" key="3">
                                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Tenetur fugiat, corporis maiores ducimus odio quae impedit similique rem quod modi ullam recusandae consequatur unde magni, rerum placeat eos error neque.</p>
                                    </Panel>
                                </Collapse>
                            </div>
                        </div>
                        <div className="review__count flex items-center justify-between">
                            <div className="review__content flex items-center">
                                <h2 className='text-xl font-bold mr-1'>{detailedJob.congViec.danhGia} Reviews</h2>
                                <Rate disabled defaultValue={3} />
                            </div>
                            <div className="review__soft">
                                <span className='text-base text-gray-500'>Sort By </span>
                                <select>
                                    <option>Most Recent</option>
                                    <option>Most Relevant</option>
                                </select>
                            </div>
                        </div>
                        <div className="review__rating flex my-4 flex-wrap">
                            <div className="rating__left">
                                <table className='border-0 w-full'>
                                    <tbody>
                                        <tr className='text-center align-baseline'>
                                            <td className='text-blue-500 text-lg w-1/4'>5 Stars</td>
                                            <td className='w-1/2'><Progress percent={95} showInfo={false} strokeColor={'orange'} /></td>
                                            <td className='text-blue-500 w-1/4'>{(detailedJob.congViec.danhGia - 1)}</td>
                                        </tr>
                                        <tr className='text-center align-baseline'>
                                            <td className='text-blue-500 text-lg'>4 Stars</td>
                                            <td className='w-1/2'><Progress percent={0} showInfo={false} strokeColor={'orange'} /></td>
                                            <td className='text-blue-500 w-1/4'>(0)</td>
                                        </tr>
                                        <tr className='text-center align-baseline'>
                                            <td className='text-blue-500  text-lg'>3 Stars</td>
                                            <td className='w-1/2'><Progress percent={0} showInfo={false} strokeColor={'orange'} /></td>
                                            <td className='text-blue-500 w-1/4'>(0)</td>
                                        </tr>
                                        <tr className='text-center align-baseline'>
                                            <td className='text-blue-500  text-lg'>2 Stars</td>
                                            <td className='w-1/2'><Progress percent={5} showInfo={false} strokeColor={'orange'} /></td>
                                            <td className='text-blue-500 w-1/4'>(1)</td>
                                        </tr>
                                        <tr className='text-center align-baseline'>
                                            <td className='text-blue-500  text-lg'>1 Stars</td>
                                            <td className='w-1/2'><Progress percent={0} showInfo={false} strokeColor={'orange'} /></td>
                                            <td className='text-blue-500 w-1/4'>(0)</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="ranking">
                                <h5 className='font-medium text-base mb-3'>Rating Breakdown</h5>
                                <ul>
                                    <li className='flex justify-between mb-3'>
                                        <p className='text-gray-400 font-bold text-base'>Seller communication level</p>
                                        <div className='flex items-center'>
                                            <i className="fa fa-star text-yellow-400" aria-hidden="true"></i>
                                            <span className='text-yellow-400'>{detailedJob.congViec.saoCongViec}</span>
                                        </div>
                                    </li>
                                    <li className='flex justify-between mb-3'>
                                        <p className='text-gray-400 font-bold text-base'>Recommend to a friend</p>
                                        <div className='flex items-center'>
                                            <i className="fa fa-star text-yellow-400" aria-hidden="true"></i>
                                            <span className='text-yellow-400'>{detailedJob.congViec.saoCongViec}</span>
                                        </div>
                                    </li>
                                    <li className='flex justify-between mb-3'>
                                        <p className='text-gray-400 font-bold text-base'>Service as described</p>
                                        <div className='flex items-center'>
                                            <i className="fa fa-star text-yellow-400" aria-hidden="true"></i>
                                            <span className='text-yellow-400'>{detailedJob.congViec.saoCongViec}</span>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="filter mb-5">
                            <h5 className='font-bold text-base mb-3'>Filter</h5>
                            <Search
                                placeholder="input search text"
                                allowClear
                                enterButton='Search'
                                size="large"
                                className='w-1/2'
                            />
                        </div>
                        <div className="review__comment border-y-2 py-4 flex items-center">
                            <div className="review__avatar mr-4 w-1/6">
                                <img style={{ borderRadius: '50%' }} className='w-full' src={detailedJob.avatar} alt="reviewer_img" />
                            </div>
                            <div className="review__coment w-5/6 space-y-3">
                                <div className='title flex items-center'>
                                    <span className='font-bold'>idarethejeff</span>
                                    <div className='flex items-center'>
                                        <i className="fa fa-star text-yellow-400" aria-hidden="true"></i>
                                        <span className='text-yellow-400'>{detailedJob.congViec.saoCongViec}</span>
                                    </div>
                                </div>
                                <div className="review__country flex items-center">
                                    <img className='w-6 mr-2' src="https://fiverr-dev-res.cloudinary.com/general_assets/flags/1f1e8-1f1ed.png" alt="country__img" />
                                    <span className='country__name'>Switzerland</span>
                                </div>
                                <p className="review__description">Lorem ipsum dolor sit amet consectetur adipisicing elit. Fugit ut dolorem enim incidunt sunt dignissimos aut tempora, reprehenderit dolore, molestiae ex commodi nulla quidem illum earum quo excepturi dolorum animi.</p>
                                <div className="like-unlike flex gap-x-2">
                                    <span className='review__judge'>Helpful?</span>
                                    <div className="like">
                                        <i class="fa fa-thumbs-up cursor-pointer"></i>
                                        <span>Yes</span>
                                    </div>
                                    <div className="unlike cursor-pointer">
                                        <i class="fa fa-thumbs-down"></i>
                                        <span>No</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="comment__list">
                            {listComment.map((comment, index) => {
                                return (
                                    <div key={index} className="comment border-b-2 py-4 flex items-center">
                                        <div className="comment__avatar mr-10 w-1/6">
                                            <img style={{ borderRadius: '50%' }} className='w-full' src={comment.avatar} alt="avatar_comment" />
                                        </div>
                                        <div className="review__coment w-5/6 space-y-3">
                                            <div className='title flex items-center'>
                                                <span className='font-bold'>{comment.tenNguoiBinhLuan}</span>
                                                <div className='flex items-center'>
                                                    <i className="fa fa-star text-yellow-400" aria-hidden="true"></i>
                                                    <span className='text-yellow-400'>{comment.saoBinhLuan}</span>
                                                </div>
                                            </div>
                                            <div className="comment__country flex items-center">
                                                <img className='w-6 mr-2' src="https://fiverr-dev-res.cloudinary.com/general_assets/flags/1f1e8-1f1ed.png" alt="country__img" />
                                                <span className='country__name'>Switzerland</span>
                                            </div>
                                            <p className="review__description">{comment.noiDung}</p>
                                            <div className="like-unlike flex gap-x-2">
                                                <span className='review__judge'>Helpful?</span>
                                                <div className="like">
                                                    <i class="fa fa-thumbs-up cursor-pointer"></i>
                                                    <span>Yes</span>
                                                </div>
                                                <div className="unlike cursor-pointer">
                                                    <i class="fa fa-thumbs-down"></i>
                                                    <span>No</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })}
                        </div>
                        <div className="add__comment my-10">
                            <div className="add__comment-title flex items-center justify-between mb-5">
                                <h5 className='font-bold text-base mb-3'>Leave some comments</h5>
                                <div className="add_rate">
                                    <Rate onChange={handleRatingChange} value={rating} />
                                    <span className='text-xl font-bold'>Rating</span>
                                </div>
                            </div>
                            <div className="add__comment-textarea">
                                <TextArea
                                    showCount
                                    maxLength={100}
                                    style={{
                                        height: '150px',
                                        marginBottom: '10px'
                                    }}
                                    value={comment}
                                    onChange={handleTextComment}
                                    placeholder='Add comment here ...'
                                />
                                <button onClick={handlePostComent} className='bg-green-500 px-3 py-2 text-white rounded'>Comment</button>
                            </div>
                        </div>
                    </div>
                    <div className='title__right border h-full'>
                        <Checkout detailedJob={detailedJob} />
                    </div>
                </div>
            </div>
            <Footer />
        </>
    )
}
