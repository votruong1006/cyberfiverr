import React from 'react'
import { NavLink } from 'react-router-dom'
import '../../../css/marketplace.css'

export default function Marketplace() {
    return (
        <div id='marketplace' className='overflow-hidden'>
            <div className="container pb-24">
                <div className="mx-auto xxl:w-11/12">
                    <h2 className='xl:text-4xl text-3xl '>Explore the marketplace</h2>
                    <div className="row justify-content-center mt-3 px-4">
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-1.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-2.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-3.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-4.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-5.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-6.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-7.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-8.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                        <div className="col-6 col-sm-6 col-md-4 col-lg-3 col-xl-2 mt-5">
                            <NavLink>
                                <div className="item flex flex-col items-center">
                                    <img src="./images/graphics-design-9.svg" alt="" />
                                    <span>Graphics & Design</span>
                                </div>
                            </NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
