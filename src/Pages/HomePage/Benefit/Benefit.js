import React, { useState } from 'react';
import '../../../css/benefit.css'
import { useDispatch } from 'react-redux';
import { handleOpenModal } from '../../../Toolkits/modalSlice';
import { dataModal } from '../../../assets/data_modal';

export default function Benefit() {
    const dispatch = useDispatch();
    let handleShowModal = (modalId) => {
        dispatch(handleOpenModal(modalId));
    }
    return (
        <div id='benefit'>
            <div className="container pt-16 pb-24 ">
                <div className="row align-items-center justify-content-between mx-auto xxl:w-11/12 lg:space-y-0 space-y-16">
                    <div className="content col-xl-5 col-lg-5 col-md-12 col-12 space-y-5">
                        <h2>A whole world of freelance talent at your fingertips</h2>
                        <ul className='space-y-5'>
                            <li className='space-y-4'>
                                <h4><i className="fa-regular fa-circle-check mr-2"></i>The best for every budget</h4>
                                <p>Find high-quality services at every price point. No hourly rates, just project-based pricing.</p>
                            </li>
                            <li className='space-y-4'>
                                <h4><i className="fa-regular fa-circle-check mr-2"></i>Quality work done quickly
                                </h4>
                                <p>Find the right freelancer to begin working on your project within minutes.</p>
                            </li>
                            <li className='space-y-4'>
                                <h4><i className="fa-regular fa-circle-check mr-2"></i>Protected payments, every time</h4>
                                <p>Always know what you'll pay upfront. Your payment isn't released until you approve the work.</p>
                            </li>
                            <li className='space-y-4'>
                                <h4><i className="fa-regular fa-circle-check mr-2"></i>24/7 support</h4>
                                <p>Questions? Our round-the-clock support team is available to help anytime, anywhere.</p>
                            </li>
                        </ul>
                    </div>
                    <div className="media col-xl-6 col-lg-6 col-md-6 col-7">
                        <button onClick={() => { handleShowModal(dataModal[4].media) }}>
                            <img src="./images/selling.png" alt="" />
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}
