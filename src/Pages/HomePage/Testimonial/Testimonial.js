import React from 'react'
import { Desktop, LargeDesktop, Mobile, SmallMobile, Tablet } from '../../../Layouts/Responsive'
import TestimonialDesktopToBelow from './TestimonialDesktopToBelow'
import TestimonialLargeDesktop from './TestimonialLargeDesktop'
import '../../../css/testimonial.css'

export default function Testimonial() {
    return (
        <>
            <SmallMobile>
                <TestimonialDesktopToBelow />
            </SmallMobile>
            <Mobile>
                <TestimonialDesktopToBelow />
            </Mobile>
            <Tablet>
                <TestimonialDesktopToBelow />
            </Tablet>
            <Desktop>
                <TestimonialDesktopToBelow />
            </Desktop>
            <LargeDesktop>
                <TestimonialLargeDesktop />
            </LargeDesktop>

        </>
    )
}
