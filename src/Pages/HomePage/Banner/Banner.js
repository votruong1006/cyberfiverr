import React from 'react'
import OwlCarousel from 'react-owl-carousel';
import '../../../css/banner.css'
import { dataBanner } from '../../../assets/data_banner';
import BannerSearch from './BannerSearch';

export default function Banner() {
    let renderContent = () => {
        return dataBanner.map((item) => {
            return (
                <img key={item.id} src={item.img} alt="" />
            )
        })
    }
    return (
        <div id='banner' >
            <BannerSearch />
            <OwlCarousel autoplay={true} mouseDrag={false} touchDrag={false} loop items={1} animateOut={'fadeOut'}>
                {renderContent()}
            </OwlCarousel>
        </div>
    )
}
