import React from 'react'
import '../../../css/bannerSearch.css'
import Search from '../../../Components/Header/Search'

export default function BannerSearch() {
    return (
        <div id='bannerSearch'>
            <div className="container text-white h-full">
                <div className="row mx-auto xxl:w-11/12 h-full">
                    <div className="content col-xl-8 col-lg-7 col-md-12 col-sm-12 col-12 flex flex-col justify-center">
                        <h2 >Find the perfect <i>freelance</i> services for your business</h2>
                        <div className='mt-4 mb-3 w-11/12'>
                            <Search />
                        </div>
                        <div className='popularSearch space-x-3'>
                            <span>Popular: </span>
                            <button className='popularSearchBtn'>Websie Design</button>
                            <button className='popularSearchBtn'>WordPress</button>
                            <button className='popularSearchBtn'>Logo Design</button>
                            <button className='popularSearchBtn'>Video Editing</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
