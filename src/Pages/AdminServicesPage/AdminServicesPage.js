import React from 'react'
import ServicesTable from './ServiceTable/ServiceTable'
import ServicesModal from './AddEditServiceModal/AddEditServiceModal'

export default function AdminServicesPage() {
    return (
        <div>
            <ServicesModal />
            <ServicesTable />
        </div>
    )
}
