import { Checkbox, Input, Tag } from "antd"

export const headerColumns = [
    {
        title: 'Mã loại công việc',
        dataIndex: 'id',
        key: 'id',
        render: (id) => {
            switch (id) {
                case 1:
                    return <Tag className="font-medium" color="green">{id}</Tag>
                    break;
                case 2:
                    return <Tag className="font-medium" color="volcano">{id}</Tag>
                    break;
                case 3:
                    return <Tag className="font-medium" color="blue">{id}</Tag>
                    break;
                case 4:
                    return <Tag className="font-medium" color="magenta">{id}</Tag>
                    break;
                case 5:
                    return <Tag className="font-medium" color="purple">{id}</Tag>
                    break;

                default:
                    return <Tag className="font-medium" color="cyan">{id}</Tag>
                    break;
            }
        }
    },
    {
        title: 'Mã công việc',
        dataIndex: 'maCongViec',
        key: 'maCongViec',
    },
    {
        title: 'Mã người thuê',
        dataIndex: 'maNguoiThue',
        key: 'maNguoiThue',

    },
    {
        title: 'Ngày thuê',
        dataIndex: 'ngayThue',
        key: 'ngayThue',
        responsive: ["md"],
    },
    {
        title: 'Trạng thái hoàn thành',
        dataIndex: 'hoanThanh',
        key: 'hoanThanh',
        responsive: ["sm"],
        render: checked => <Checkbox checked={checked} />
    },
    {
        title: 'Action',
        dataIndex: 'action',
        key: 'action',
    },
]
