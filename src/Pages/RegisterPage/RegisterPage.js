import React from "react";
import { Button, Checkbox, Form, Input, Radio, message } from "antd";
import { userSer } from "../../Services/userService";
import { useNavigate } from "react-router";
import { NavLink } from "react-router-dom";
import { useDispatch } from 'react-redux'
import { setHomePage } from '../../Toolkits/HomeSlice';

export default function RegisterPage() {
    const [form] = Form.useForm();
    const navigate = useNavigate()
    let dispatch = useDispatch();
    dispatch(setHomePage());
    const onFinish = (values) => {

        userSer
            .postSingup(values)
            .then((res) => {
                message.success("Đăng kí thành công !")
                navigate("/login")
                console.log(res);
            })
            .catch((err) => {
                console.log(err);
            });
    };
    return (
        <div style={{ width: "900px", boxShadow: "0 15px 16.83px 0.17px rgba(0,0,0,.05)", border: "1px solid rgba(0,0,0,.05)", marginTop: '150px', marginBottom: '48px' }} className="container mx-auto rounded-2xl mb-14 mt-10">
            <div className="flex-wrap flex">
                <div className="pl-9 pr-3 register__left">
                    <h2 className="mb-8 ml-28 font-medium text-3xl text-green-500">Register</h2>
                    <Form
                        form={form}
                        name="register"
                        onFinish={onFinish}
                        initialValues={{
                            residence: ["zhejiang", "hangzhou", "xihu"],
                            prefix: "86",
                        }}
                        className="w-full "
                        scrollToFirstError
                        layout="vertical"
                        wrapperCol={24}
                        labelCol={24}
                        autoComplete="off"
                    >

                        <Form.Item
                            className=""
                            name="name"
                            rules={[
                                {
                                    required: true,
                                    message: "Vui lòng nhập tài khoản !",
                                    whitespace: true,
                                },
                            ]}
                        >
                            <Input className=" text-lg font-normal" placeholder="Your name" />
                        </Form.Item>

                        <Form.Item
                            name="email"
                            rules={[
                                {
                                    type: "email",
                                    message: "Chưa đúng định dạng email !",
                                },
                                {
                                    required: true,
                                    message: "Vui lòng nhập email của bạn !",
                                },
                            ]}
                        >
                            <Input className="text-lg font-normal" placeholder="Your email" />
                        </Form.Item>

                        <Form.Item
                            name="password"
                            rules={[
                                {
                                    required: true,
                                    message: "Vui lòng nhập mập khẩu!",
                                },
                                {
                                    min: 8,
                                    message: "Tối thiểu 8 kí tự",
                                },
                            ]}
                        >
                            <Input.Password
                                className="text-lg font-normal"
                                placeholder="Your password"
                            />
                        </Form.Item>

                        <Form.Item
                            name="confirm"
                            dependencies={["password"]}
                            rules={[
                                {
                                    required: true,
                                    message: "Vui lòng xác nhận mật khẩu !",
                                },
                                ({ getFieldValue }) => ({
                                    validator(_, value) {
                                        if (!value || getFieldValue("password") === value) {
                                            return Promise.resolve();
                                        }
                                        return Promise.reject(
                                            new Error("Mật khẩu chưa trùng khợp !")
                                        );
                                    },
                                }),
                            ]}
                        >
                            <Input.Password
                                className="text-lg font-normal"
                                placeholder="Repeat your password"
                            />
                        </Form.Item>
                        <Form.Item
                            name="phone"
                            rules={[
                                {
                                    type: "string",
                                    message: "Phone phải từ 03 05 07 08 09 và có 10 số",
                                },
                                {
                                    required: true,
                                    message: "Vui lòng nhập số điện thoại của bạn !",
                                },
                            ]}
                        >
                            <Input className="text-lg font-normal" placeholder="Your phone" />
                        </Form.Item>
                        <Form.Item
                            name="birthday"
                            rules={[
                                {
                                    required: true,
                                    message: "Birthday không được bỏ trống",
                                },
                            ]}
                        >
                            <Input
                                className="text-lg font-normal"
                                placeholder="Your birthday"
                                type="date"
                            />
                        </Form.Item>
                        <Form.Item label="">
                            <Radio.Group>
                                <Radio className="text-2xl" value="male">
                                    {" "}
                                    Male{" "}
                                </Radio>
                                <Radio value="female"> Female </Radio>
                            </Radio.Group>
                        </Form.Item>
                        <Form.Item name="remember" valuePropName="checked">
                            <Checkbox>
                                I agree all statements in <a className="text-blue-500 hover:text-blue-400" style={{ textDecoration: "none" }} href="#">Terms of service</a>
                            </Checkbox>
                        </Form.Item>
                        <Form.Item>
                            <Button
                                type="primary"
                                className="text-center  font-medium bg-green-500 "
                                htmlType="submit"
                            >
                                Submit
                            </Button>
                        </Form.Item>
                    </Form>
                </div>
                <div className="mx-14 mt-12 register__left ">
                    <img className="w-full" src="https://demo5.cybersoft.edu.vn/static/media/signup.bd994738c4eb8deb2801.jpg" alt="" />
                    <NavLink style={{ textDecoration: "none" }} className="no-underline" to="/login"> <a style={{ textDecoration: "none" }} className="text-sm text-green-400 hover:text-blue-400 no-underline text-center mt-10 block" href="">I am already member</a></NavLink>
                </div>
            </div>
        </div>
    );
}
